/**
 * @author:稀饭
 * @time:上午12:03:04
 * @filename:Location.java
 */
package ssh.rice.self.entity;

public class Area {
	private int aid;
	private String place;
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Area() {
		super();
	}
}
