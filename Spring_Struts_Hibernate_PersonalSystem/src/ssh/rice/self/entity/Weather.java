/**
 * @author:稀饭
 * @time:上午12:23:10
 * @filename:weather.java
 */
package ssh.rice.self.entity;

import java.io.Serializable;

public class Weather implements Serializable {
	private static final long serialVersionUID = 1L;
	private String dateNum;
	private String label1;
	private String label2;
	private String temperature;

	public String getDateNum() {
		return dateNum;
	}

	public void setDateNum(String dateNum) {
		this.dateNum = dateNum;
	}

	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}

	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public Weather() {
		super();
	}

	public Weather(String dateNum, String label1, String label2,
			String temperature) {
		super();
		this.dateNum = dateNum;
		this.label1 = label1;
		this.label2 = label2;
		this.temperature = temperature;
	}

}
