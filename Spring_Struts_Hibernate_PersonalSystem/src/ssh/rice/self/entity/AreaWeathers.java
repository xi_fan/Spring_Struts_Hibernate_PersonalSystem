/**
 * @author:稀饭
 * @time:上午10:17:21
 * @filename:AreaWeathers.java
 */
package ssh.rice.self.entity;

import java.util.concurrent.LinkedBlockingQueue;

public class AreaWeathers {
	private LinkedBlockingQueue<Weather> weathers;
	private String area;

	public AreaWeathers() {
		super();
	}

	public LinkedBlockingQueue<Weather> getWeathers() {
		return weathers;
	}

	public void setWeathers(LinkedBlockingQueue<Weather> weathers) {
		this.weathers = weathers;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
}
