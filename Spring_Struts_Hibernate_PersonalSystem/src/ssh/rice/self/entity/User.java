/**
 * @author:稀饭
 * @time:上午12:23:10
 * @filename:weather.java
 */
package ssh.rice.self.entity;

import java.util.HashSet;
import java.util.Set;

public class User {
	private int uid;
	private String username;
	private String password;
	private Set<Area> areas = new HashSet<Area>();

	public User(String username, String password) {

		this.username = username;
		this.password = password;
	}

	public User() {
		super();
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Area> getAreas() {
		return areas;
	}

	public void setAreas(Set<Area> areas) {
		this.areas = areas;
	}

}
