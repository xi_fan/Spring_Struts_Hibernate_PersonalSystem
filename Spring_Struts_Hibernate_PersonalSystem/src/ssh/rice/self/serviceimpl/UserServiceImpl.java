/**
 * @author:稀饭
 * @time:����11:58:34
 * @filename:UserAcition.java
 */
package ssh.rice.self.serviceimpl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ssh.rice.self.dao.UserDao;
import ssh.rice.self.entity.Area;
import ssh.rice.self.entity.User;
import ssh.rice.self.service.UserService;

public class UserServiceImpl implements UserService {
	@Autowired
	@Qualifier("userDao")
	private UserDao userDao;

	// public void setUserDao(UserDao userDao) {
	// this.userDao = userDao;
	// }

	/**
	 * @Title: login
	 * @Description: TODO
	 * @return
	 */
	@Override
	public int login(User user) {
		return userDao.selectWhere(user);
	}

	/**
	 * @Title: register
	 * @Description: TODO
	 * @return
	 */
	public String register() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @Title: getAreas
	 * @Description: TODO
	 * @param uid
	 * @return
	 */
	public Set<Area> getAreas(int uid) {
		return userDao.getAreas(uid);
	}
}
