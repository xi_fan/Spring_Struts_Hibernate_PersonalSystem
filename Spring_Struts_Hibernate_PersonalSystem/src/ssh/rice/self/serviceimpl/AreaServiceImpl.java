/**
 * @author:稀饭
 * @time:下午11:14:20
 * @filename:AreaServiceImpl.java
 */
package ssh.rice.self.serviceimpl;

import ssh.rice.self.dao.AreaDao;
import ssh.rice.self.entity.Area;
import ssh.rice.self.service.AreaService;

public class AreaServiceImpl implements AreaService {

	private AreaDao areaDao;

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

	/**
	 * @Title: add
	 * @Description: TODO
	 * @return
	 */
	@Override
	public int add(Area area, int uid) {
		// TODO Auto-generated method stub
		return areaDao.add(area, uid);
	}
}
