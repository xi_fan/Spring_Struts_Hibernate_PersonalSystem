/**
 * @author:ϡ��
 * @time:����12:20:33
 * @filename:UserAction.java
 */
package ssh.rice.self.service;

import java.util.Set;

import ssh.rice.self.entity.Area;
import ssh.rice.self.entity.User;

public interface UserService {
	public int login(User user);

	public String register();

	public Set<Area> getAreas(int uid);
}
