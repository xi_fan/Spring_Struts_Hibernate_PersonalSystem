/**
 * @author:稀饭
 * @time:下午11:06:29
 * @filename:AreaService.java
 */
package ssh.rice.self.service;

import java.util.Set;

import ssh.rice.self.entity.Area;

public interface AreaService {
	public int add(Area area, int uid);
}
