/**
 * @author:稀饭
 * @time:上午8:02:58
 * @filename:AreaDao.java
 */
package ssh.rice.self.dao;

import java.util.Set;

import ssh.rice.self.entity.Area;
import ssh.rice.self.entity.User;

public interface UserDao {
	public int selectWhere(User user);

	public void add_OneToOne(User user);

	public Set<Area> getAreas(int uid);
}
