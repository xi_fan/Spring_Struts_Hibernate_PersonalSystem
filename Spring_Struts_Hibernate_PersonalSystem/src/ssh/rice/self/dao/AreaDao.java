/**
 * @author:稀饭
 * @time:上午8:02:58
 * @filename:AreaDao.java
 */
package ssh.rice.self.dao;

import ssh.rice.self.entity.Area;

public interface AreaDao {
	public int add(Area area, int uid);
}
