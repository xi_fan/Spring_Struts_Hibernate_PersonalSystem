/**
 * @author:稀饭
 * @time:下午8:13:48
 * @filename:HibernateUtils.java
 */
package ssh.rice.self.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class HibernateUtils {

	private SessionFactory sessionFactory;
	private Session session;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public HibernateUtils() {

	}

	/**
	 * 获取Session对象
	 * 
	 * @return the session
	 */
	public Session getSession() {
		return sessionFactory.openSession();
	}

	/**
	 * 关闭session
	 */
	public void closeSession() {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	/**
	 * 关闭制定Session对象
	 */
	public void closeSession(Session session) {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}
}
