/**
 * @author:稀饭
 * @time:����11:58:34
 * @filename:UserAcition.java
 */
package ssh.rice.self.action;

import ssh.rice.self.entity.User;
import ssh.rice.self.service.UserService;

import com.opensymphony.xwork2.ModelDriven;

public class UserAction extends SuperAction implements ModelDriven<User> {

	private static final long serialVersionUID = 1L;
	private User user = new User();
	private UserService userService = (UserService) applicationContext
			.getBean("userService");

	// public void setUserService(UserService userService) {
	// this.userService = userService;
	// }

	/**
	 * @Title: login
	 * @Description: TODO
	 * @return
	 */
	public String login() {
		int login_id = userService.login(user);
		if (login_id == -1) {
			return "login";
		} else {
			session.setAttribute("loginUserName", user.getUsername());
			session.setAttribute("login_id", login_id);
			return "success";
		}
	}

	/**
	 * @Title: getModel
	 * @Description: TODO
	 * @return
	 */
	public User getModel() {
		// TODO Auto-generated method stub
		return user;
	}

	/**
	 * @Title: register
	 * @Description: TODO
	 * @return
	 */
	public String register() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @Title: logout
	 * @Description: TODO
	 * @return
	 */
	public String logout() {
		// TODO Auto-generated method stub
		session.removeAttribute("login_id");
		session.removeAttribute("loginUserName");
		return "login";
	}

}
