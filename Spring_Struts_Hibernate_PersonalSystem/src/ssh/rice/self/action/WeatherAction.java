/**
 * @author:稀饭
 * @time:上午12:31:17
 * @filename:WeatherAction.java
 */
package ssh.rice.self.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import ssh.rice.self.entity.Area;
import ssh.rice.self.entity.AreaWeathers;
import ssh.rice.self.entity.Weather;
import ssh.rice.self.service.AreaService;
import ssh.rice.self.service.UserService;
import ssh.rice.self.util.ConnectionUtil;

import com.opensymphony.xwork2.ModelDriven;

public class WeatherAction extends SuperAction implements ModelDriven<Area> {

	private Area area = new Area();
	private int uid;
	private UserService userService = (UserService) applicationContext
			.getBean("userService");
	private AreaService areaService = (AreaService) applicationContext
			.getBean("areaService");
	private static final long serialVersionUID = 1L;
	private ConnectionUtil connectionUtil = (ConnectionUtil) applicationContext
			.getBean("connectionUtil");

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public ConnectionUtil getConnectionUtil() {
		return connectionUtil;
	}

	public void setConnectionUtil(ConnectionUtil connectionUtil) {
		this.connectionUtil = connectionUtil;
	}

	public LinkedBlockingQueue<Weather> getWeathers(String area, int index) {

		String[] requreString = {
				"Accept: text/html, application/xhtml+xml, */*",
				"Referer: http://www.weather.com.cn/",
				"User-Agent: " + connectionUtil.user_agent[index],
				"Host: www.weather.com.cn", "Connection: Keep-Alive" };
		LinkedBlockingQueue<Weather> weatherList = new LinkedBlockingQueue<Weather>();
		String result = connectionUtil.sendGet(
				"http://toy1.weather.com.cn/search?cityname=" + area,
				requreString);
		Pattern pattern = Pattern.compile("\\d{9}");
		Matcher matcher = pattern.matcher(result);
		String label = null;
		if (matcher.find()) {
			label = matcher.group();
			result = connectionUtil.sendGet(
					"http://www.weather.com.cn/weather/" + label + ".shtml",
					requreString);
			Document document = Jsoup.parse(result);
			Elements element = document.select("ul");
			String[] data = element.get(2).text().split(" ");
			for (int i = 0; i < data.length / 4; i++) {

				Weather weather = new Weather(data[i * 4], data[i * 4 + 1],
						data[i * 4 + 2], data[i * 4 + 3]);
				weatherList.add(weather);
			}
		}
		return weatherList;
	}

	/**
	 * @Title: getLocalWeather
	 * @Description: TODO
	 * @param @return
	 * @return String
	 */
	public String getLocalWeather() {
		LinkedBlockingQueue<Weather> weatherList = getWeathers("广州", 0);
		session.setAttribute("weatherList", weatherList);
		return "weather";
	}

	/**
	 * @Title: getOthersWeather
	 * @Description: TODO
	 * @param @return
	 * @return String
	 */
	public String getOthersWeather() {
		LinkedBlockingQueue<AreaWeathers> weathersList = new LinkedBlockingQueue<AreaWeathers>();
		uid = (Integer) session.getAttribute("login_id");
		Set<Area> set = userService.getAreas(uid);
		Iterator<Area> iterator = set.iterator();
		int index = uid;
		System.out.println("uid=" + uid);
		while (iterator.hasNext()) {
			index++;
			Area area = iterator.next();
			String place = area.getPlace();
			LinkedBlockingQueue<Weather> weathers = getWeathers(place,
					index % 8);
			AreaWeathers areaWeathers = new AreaWeathers();
			areaWeathers.setWeathers(weathers);
			areaWeathers.setArea(place);
			weathersList.add(areaWeathers);
		}
		session.setAttribute("weathersList", weathersList);
		return "weather_others";
	}

	public String addLocation() {
		uid = (Integer) session.getAttribute("login_id");
		areaService.add(area, uid);
		return NONE;
	}

	/**
	 * @Title: getLocation
	 * @Description: TODO
	 * @param @return
	 * @return String
	 */
	public String getLocation() {
		// 获取传参
		int index = areaService.add(area, uid);
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.print(index);
		return NONE;
	}

	/**
	 * @Title: getModel
	 * @Description: TODO
	 * @return
	 */
	@Override
	public Area getModel() {
		// TODO Auto-generated method stub
		return area;
	}
}
